<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index()
    {   
        $title = "Halaman Register";
        return view('register', [ 'title' => $title ]);
    }
    public function welcome(Request $request)
    {   
        $title = "Hamalan Welcome";
        $first_name = $request['first_name'];
        $last_name = $request['last_name'];
        return view('welcome', compact('title','first_name','last_name'));
    }
}
